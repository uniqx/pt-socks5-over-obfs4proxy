---
- hosts: all
  become_method: sudo

  tasks:
    - name: "apt: install debian packages"
      apt:
        name: "{{item}}"
        state: latest
        autoclean: yes
        autoremove: yes
        install_recommends: no
        update_cache: yes
      with_items:
        - obfs4proxy
        - unattended-upgrades
        - dnsutils
        - libcap2-bin
        - iptables-persistent
        - dante-server
      become: yes

    - copy:
        dest: /etc/iptables/rules.v4
        content: |
            *filter
            :INPUT DROP [0:0]
            :FORWARD ACCEPT [0:0]
            :OUTPUT ACCEPT [0:0]
            -A INPUT -i lo -j ACCEPT
            -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
            -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
            -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT
            -A INPUT -p icmp --icmp-type 8 -j ACCEPT
            COMMIT
      become: yes
      notify: reload_iptables
    - copy:
        dest: /etc/iptables/rules.v6
        content: |
            *filter
            :INPUT DROP [0:0]
            :FORWARD ACCEPT [0:0]
            :OUTPUT ACCEPT [0:0]
            -A INPUT -i lo -j ACCEPT
            -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
            -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
            -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT
            -A INPUT -p icmpv6 --icmpv6-type 128 -j ACCEPT
            COMMIT
      become: yes
      notify: reload_iptables

    - user:
        name: obfs4
        createhome: yes
      become: yes
    - copy:
        src: obfs4_server.sh
        dest: /home/obfs4/obfs4_server.sh
        mode: 0700
        owner: obfs4
        group: obfs4
      become: yes
    - name: "allow obfs4 to bind privileged network ports"
      capabilities:
        path: /usr/bin/obfs4proxy
        capability: cap_net_bind_service=+eip
        state: present
      become: yes
    - copy:
        dest: '/etc/systemd/system/obfs4proxy.service'
        owner: root
        group: root
        mode: 0755
        content: |
            # file is managed via ansible

            [Unit]
            Description=OBFS 4 Proxy Server

            [Service]
            ExecStart=/home/obfs4/obfs4_server.sh
            User=obfs4
            Group=obfs4

            [Install]
            WantedBy=multi-user.target
      become: yes
      notify: restart_obfs4proxy


    - name:
      copy:
        dest: "/etc/danted.conf"
        content: |
            logoutput: /var/log/socksd.log

            internal: 127.0.0.1 port = 9595
            external: {{ ansible_default_ipv4.address }}

            user.privileged: proxy
            user.unprivileged: nobody
            user.libwrap: nobody

            srchost: nodnsmismatch

            clientmethod: none
            socksmethod: none

            client pass {
              from: 127.0.0.1/32 to: 0.0.0.0/0
              log: error
            }

            socks pass {
              from: 0.0.0.0/0 to: 0.0.0.0/0
              command: bind connect udpassociate
              log: error
            }
        owner: root
        group: root
        mode: 0644
      become: yes
      notify: restart_danted

  handlers:
    - name: restart_obfs4proxy
      systemd: name=obfs4proxy.service state=restarted enabled=yes daemon_reload=yes
      become: yes
    - name: reload_iptables
      command: "/usr/sbin/netfilter-persistent start"
      become: yes
    - name: restart_danted
      systemd: name=danted.service state=restarted enabled=yes
      become: yes
